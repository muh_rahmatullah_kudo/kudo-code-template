# Android Kudo code template

This is code template for boosting your productivity, often times when we're working a feature, we need to write or copy some boilerplate code, like creating a screen, you will need to create activity, viewmodel, xml layout with databinding and so on. This is a simple task yet it took a few seconds and we do it over and over again. what if we can automate the process with one click and will create all the necessary code for you ? this Kudo code template here to serve you.

## How to use

With Kudo code template we can easily get rid of writting a boilerplate code when working in android studio, say you wanted to create new screen, only need to go to :

**Right click on the package you wanted to create at->new->Kudo->KudoActivity + ViewModel**

After that you will be shown a wizard to fill the name of the Activity, xml layout, you can even chose the orientation of the screen(Still many to come) not only that, it will automatically add the layout in manifest file. (You only need to fill the activity name tho, all field will automatically filled)

After doing all of that, it will generated the code for you, you may need to rebuild sometimes, since it will triggered the databinding to create a generated file but other than that it works like a charm.

## Demo
For the demo, you can check this link = https://drive.google.com/file/d/1m2dYJvLq8Z6Jp7fz7znr-gU_S4Ufd-19/view?usp=sharing

## Generated Code

Here is sample generated code :
* Activity
```
package kudo.mobile.app.ovotuva.features.kudoform;

import android.os.Bundle;
import android.view.MenuItem;

import kudo.mobile.app.common.base.KudoBaseActivity;
import kudo.mobile.app.ovotuva.R;
import kudo.mobile.app.ovotuva.BR;
import kudo.mobile.app.ovotuva.databinding.ActivityKudoFormBinding;

public class KudoFormActivity extends KudoBaseActivity<ActivityKudoFormBinding, KudoFormViewModel> {

    @Override
    public int getViewModelBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_kudo_form;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar("KudoFormActivity", false, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
```
* ViewModel
```
package kudo.mobile.app.ovotuva.features.kudoform;

import kudo.mobile.base.BaseViewModel;
import kudo.mobile.app.analytic.core.KudoAnalytic;

import javax.inject.Inject;

public class KudoFormViewModel extends BaseViewModel {

    private KudoAnalytic mKudoAnalytic;

    @Inject
    public KudoFormViewModel(KudoAnalytic KudoAnalytic) {
        mKudoAnalytic = KudoAnalytic;
    }

}
```

* xml layout
```
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>
        <variable
            name="viewModel"
            type="kudo.mobile.app.ovotuva.features.kudoform.KudoFormViewModel" />
    </data>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"></LinearLayout>

</layout>
```

## Import this template

Here are the step :

* Clone this repository
* Copy the folder
* Open finder, go to application -> right click on android studio -> show package contents
* Paste it to  Contents/plugins/android/lib/templates/activities
* if you already open AS, restart it
* you're good to go

## Done

* [x] KudoActivity and ViewModel

## Todo

* [ ] Data Source
* [ ] Repository
* [ ] Adapter
* [ ] Rest
* [ ] Write separate template for kotlin
(Now if we pick kotlin, it will convert the code from java to kotlin using built in feature but it is still okay)

Any suggestions ? please create issue ? or please create pull request :) contribution are very highly appreciated

## Author

* **Muhammad Rahmatullah**

muh.rahmatullah@kudo.co.id





