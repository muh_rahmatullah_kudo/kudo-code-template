<manifest xmlns:android="http://schemas.android.com/apk/res/android" 
  >
  <application
      >
    <activity 
        android:name="${relativePackage}.${packName}.${className}"
        <#if isPortrait>android:screenOrientation="sensorPortrait"</#if>/>
  </application>
</manifest>