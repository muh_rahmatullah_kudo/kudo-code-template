package ${packageName}.${packName};

import android.os.Bundle;
import android.view.MenuItem;
import kudo.mobile.app.common.base.KudoBaseActivity;
<#if applicationPackage??>import ${applicationPackage}.R;</#if>
<#if applicationPackage??>import ${applicationPackage}.BR;</#if>
<#if applicationPackage??>import ${applicationPackage}.databinding.Activity${Name}Binding;</#if>

public class ${className} extends KudoBaseActivity<Activity${Name}Binding, ${Name}ViewModel>{

  @Override
  public int getViewModelBindingVariable() {
        return BR.viewModel;
    }

  @Override public int getLayoutId() {
      return R.layout.${activityName};
  }

  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar("${className}", false, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
