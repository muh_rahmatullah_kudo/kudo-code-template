package ${packageName}.${packName};

import kudo.mobile.base.BaseViewModel;
import kudo.mobile.app.analytic.core.KudoAnalytic;

import javax.inject.Inject;

public class ${Name}ViewModel extends BaseViewModel{

    private KudoAnalytic mKudoAnalytic;
    @Inject
    public ${Name}ViewModel(KudoAnalytic KudoAnalytic) {
        mKudoAnalytic = KudoAnalytic;
    }

}
